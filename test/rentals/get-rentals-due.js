require("chai-as-promised");

var should = require('chai').should();

var ddb = require('../../index.js');

function getta(date) {
    return `${date.getFullYear()}${('0' + (date.getMonth() + 1)).slice(-2)}${('0' + date.getDate()).slice(-2)}` | 0;
}

module.exports = function() {
    return function getRentalsDue() {
        it('should return due rentals', function() {
            return ddb.getRentalsDue(getta(new Date())).then(items => items.map(x => x.rental_id).find(x => x === 'o790B')).should.eventually.equal('o790B');
        });
    }
}
