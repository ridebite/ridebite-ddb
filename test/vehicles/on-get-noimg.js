var ddb = require('../../index.js');

var chai = require('chai');
var should = chai.should();
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

module.exports = function (userId) {
	return function onGetNoImg() {
	    it('should equal 1', function () {
	        return ddb.getNoImgVehicles().then(vs => 
	        	{
	        		return vs.length;
	        	}).should.eventually.equal(1);
	    });
	}
}