var ddb = require('../index.js');

const rentalRequest = {
    date: 20160316,
    driver: {
        email: 'driver@hertigcarl.se',
        name: 'Fabbe Förare',
        phone: '+46739332499',
        pictureUrl: 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTZMs8TIim0HSwMtgeny01NDM-Fe9MbXEoZGKVBdx9cVzbCrq9o',
        stripe_account_id: 'acct_17peJCK7f2db0eiE',
        stripe_cus: 'cus_85pzmQwpPdOQev',
        user_id: '3H8kt'
    },
    upcoming: 1,
    driver_id: '3H8kt',
    expired: 1458897339931,
    owner: {
        email: 'owner@hertigcarl.se',
        has_valid_license: true,
        name: 'Hubbe Hyrare',
        persno: '198104270197',
        phone: '+46739332499',
        pictureUrl: 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTZMs8TIim0HSwMtgeny01NDM-Fe9MbXEoZGKVBdx9cVzbCrq9o',
        stripe_account_id: 'acct_17peIwCXgTo4ZDTX',
        stripe_cus: 'cus_85pyiWtS3eQzWx',
        user_id: '1Lsr65'
    },
    owner_id: '1Lsr65',
    owner_rejected: 'Wed Mar 23 2016 18:53:01 GMT+0100 (CET)',
    receipt: {
        application_fee: 530,
        discount: 'ridebite2016',
        payed_by_driver: 1970,
        payed_to_owner: 1440,
        price_set_by_owner: 1800
    },
    regno: 'JLL098',
    requested: 1458749020195,
    reservations: {
        '1cefgs': 20160318,
        o790A: 20160316,
        Z1JpRgr: 20160317,
        ZViL0z: 20160319
    },
    vehicle: {
        carfax_report: 'http://was.carfax.eu/icr/?dealer=5100008895&vinreg=JLL098&checksum=7823cd6ba6e9173193aa5abe33b5927d',
        fuel: 'Bensin',
        hp: '63',
        id: 'Z2tW6ko',
        information: 'Lorem ipsum osv...',
        lat: 15.0123,
        lng: 24.1322,
        make: 'Suzuki',
        model: 'Gsx750',
        price_amount: 450,
        price_currency: 'SEK',
        price_unit: 'day',
        registration_number: 'JLL098',
        user_id: '1Lsr65',
        year: '1999'
    },
    vehicle_id: 'Z2tW6ko'
};

describe('Reservations', function () {
    this.timeout(60000);

    beforeEach(function () {
        return ddb.createTables()
            .then(() => ddb.makeReservations([
            {
                date: 20160316,
                id: 'o790B',
                upcoming: 1,
                vid: 'Z2tW6ky'
            },
            {
                date: 20160316,
                id: 'o790A',
                upcoming: 1,
                rental_id: 'ayG3e',
                vid: 'Z2tW6ko'
            },

            {
                date: 20160317,
                id: 'Z1JpRgr',
                upcoming: 1,
                rental_id: 'ayG3e',
                vid: 'Z2tW6ko'
            },
            {
                date: 20160318,
                id: '1cefgs',
                upcoming: 1,
                rental_id: 'ayG3e',
                vid: 'Z2tW6ko'
            },
            {
                date: 20160319,
                id: 'ZViL0z',
                upcoming: 1,
                rental_id: 'ayG3e',
                vid: 'Z2tW6ko'
            }
            ]))
            .then(() => ddb.update('rental_request', 'o790A', rentalRequest));
    });

    describe('on-delete-reservations', require('./reservations/on-delete-reservations')(rentalRequest.reservations));
    describe('on-delete-single-when-rented', require('./reservations/on-delete-single-when-rented')('o790A'));
    describe('on-delete-single-not-rented', require('./reservations/on-delete-single-not-rented')('o790B'));
});
