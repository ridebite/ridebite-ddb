var cleanProperties = require('../lib/helpers/clean-properties');
describe('Clean Properties', function () {
    describe('when-all-is-good', require('./clean-properties/when-all-is-good')(cleanProperties));
    describe('when-a-prop-is-undefined', require('./clean-properties/when-a-prop-is-undefined')(cleanProperties));
    describe('when-a-prop-is-null', require('./clean-properties/when-a-prop-is-null')(cleanProperties));
});
