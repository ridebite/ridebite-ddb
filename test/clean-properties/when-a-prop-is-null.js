require("chai-as-promised");

var should = require('chai').should();

module.exports = function (cleanProperties) {
	return function whenAPropIsUndefined() {
    var result;
    before(() => {
      result = cleanProperties({
        name: null,
      });
    });
    it('should remove property name', function () {
			should.not.exist(result.name);
    });
	}
}
