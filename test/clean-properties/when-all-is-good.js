require("chai-as-promised");

var should = require('chai').should();

module.exports = function (cleanProperties) {
	return function whenAllIsGood() {
    var result;
    before(() => {
      result = cleanProperties({
        name: 'Testcase',
        status: true
      });
    });
    it('should preserve name property value', function () {
        result.name.should.equal('Testcase');
    });
    it('should preserve status property value', function () {
        result.name.should.be.truthy;
    });
	}
}
