var ddb = require('../index.js');
describe('Book', function () {
    this.timeout(60000);
    
    beforeEach(function () {
        return ddb.createTables();
    });

    //describe('on-book', require('./book/on-book')({
    //    "booking_id": "very-unique",
    //    "date": "20160225",
    //    "timespan": 3,
    //    "pickup" : "13:21",
    //    "return" : "14:23",
    //    "location_lat" : 34.12,
    //    "location_lng" : 89.09,
    //    "category" : "El Grande"
    //}));

    describe('get-bookings', require('./book/get-bookings')());
});
