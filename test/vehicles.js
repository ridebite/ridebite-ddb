var ddb = require('../index.js');
describe('Vehicles', function () {
    this.timeout(60000);
    
    beforeEach(function () {
        return ddb.createTables()
            .then(() => ddb.putItem('vehicle', {
              "carfax_report": "http://was.carfax.eu/icr/?dealer=5100008895&vinreg=ASD123&checksum=e4ec84eace7bde9cfdacd93956df32d3",
              "fuel": "Bensin",
              "hp": "63",
              "id": "2i5ivm",
              "make": "Suzuki",
              "model": "Gsx750",
              "noimage": 1,
              "registration_number": "ASD123",
              "user_id": "12d6u9",
              "year": "1999"
            }));
    });

    describe('on-get-noimg', require('./vehicles/on-get-noimg')());
});
