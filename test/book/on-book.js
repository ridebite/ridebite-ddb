require("chai-as-promised");

var should = require('chai').should();

var ddb = require('../../index.js');

module.exports = function (booking) {
	return function onBook() {
	    it('should save booking', function () {
	        return ddb.putItem('booking', booking).should.be.fulfilled;
	    });
	}
}
