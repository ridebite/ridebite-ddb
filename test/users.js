var ddb = require('../index.js');
describe('Users', function () {
    this.timeout(60000);
    
    beforeEach(function () {
        return ddb.createTables()
            .then(() => ddb.putItem('user', {
                    user_id: 'user_1',
                    name: 'name_1',
                    firstname: 'firstname_1',
                    email: 'email_1',
                    pictureUrl: 'pictureUrl_1',
                    stripe_account_id: 'stripe_account_id_1',
                    stripe_cus: 'stripe_cus_1'
                }))
            .then(() => ddb.putItem('user', {
                    user_id: 'user_2',
                    name: 'name_2',
                    firstname: 'firstname_2',
                    email: 'email_2',
                    pictureUrl: 'pictureUrl_2',
                    ref: 'user_1',
                    stripe_account_id: 'stripe_account_id_2',
                    stripe_cus: 'stripe_cus_2'
            }));
    });

    describe('on-get-refs', require('./users/on-get-refs')('user_1'));
});
