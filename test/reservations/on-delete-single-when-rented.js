var ddb = require('../../index.js');

module.exports = function (reservationId) {
	return function onDeleteSingleWhenRented() {
	    it('should fail', function () {
	        return ddb.deleteReservation(reservationId).should.be.rejected;
	    });
	}
}