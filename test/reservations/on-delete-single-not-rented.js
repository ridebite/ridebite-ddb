var ddb = require('../../index.js');

module.exports = function (reservationId) {
	return function onDeleteSingleNotRented() {
	    it('should succeed', function () {
	        return ddb.deleteReservation(reservationId).should.be.fulfilled;
	    });
	}
}