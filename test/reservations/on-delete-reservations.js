require("chai-as-promised");

var should = require('chai').should();

var ddb = require('../../index.js');

module.exports = function (reservations) {
	return function onDeleteRentalRequests() {
	    it('should delete reservations', function () {
	        return ddb.deleteReservations(reservations).should.be.fulfilled;
	    });
	}
}
