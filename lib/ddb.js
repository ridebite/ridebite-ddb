'use strict';

if (!process.env.AWS_ACCESS_KEY || !process.env.AWS_SECRET_KEY) {
    throw new Error('Make sure you have AWS_ACCESS_KEY and AWS_SECRET_KEY among your environment variables.');
}

const AWS = require('aws-sdk');

AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
    region: 'eu-west-1',
});

module.exports.AWS = AWS;

const swear = require('promise-handler').es6;
const debug = require('debug')('ridebite-ddb:ddb');

let dynasty = require('dynasty')({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
    region: 'eu-west-1',
});

let ddb = require('dynamodb').ddb({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
    endpoint: 'dynamodb.eu-west-1.amazonaws.com',
});

const renderTableName = require('./prefix');

const cleanProperties = require('./helpers/clean-properties');

function createTable(params) {
    console.log(`creating ${params.TableName}`);
    const dynamodb = new AWS.DynamoDB();
    return swear(handler => dynamodb.createTable(params, handler)).then(() => {
        var params2 = {
            TableName: params.TableName,
        };
        console.log(`waiting for ${params2.TableName} to finish`);
        return swear(waitHandler => dynamodb.waitFor('tableExists', params2, waitHandler));
    });
}

function updateTable(params) {
    const dynamodb = new AWS.DynamoDB();
    return swear(handler => dynamodb.updateTable(params, handler));
}

function putItem(table, item) {
    table = renderTableName(table);
    return new Promise((resolve, reject) => {
        item = cleanProperties(item);

        ddb.putItem(table, item, {}, function(err, res, cap) {
            if (err) {
                return reject(err);
            }

            return resolve(res);
        });
    });
}

function getItem(table, hash) {
    table = renderTableName(table);
    let items = dynasty.table(table);
    return items.find(hash);
}

function getItemWithMap(table, hash) {
    table = renderTableName(table);
    return new Promise((resolve, reject) => {
        var dynamodbDoc = new AWS.DynamoDB.DocumentClient();

        var params = {
            TableName: table,
            KeyConditionExpression: '#id = :iidd',
            ExpressionAttributeNames: {
                '#id': 'user_id',
            },
            ExpressionAttributeValues: {
                ':iidd': hash,
            },
        };

        dynamodbDoc.query(params, function(err, data) {
            if (err) {
                console.error('Unable to query. Error:', JSON.stringify(err, null, 2));
                return reject(err);
            } else {
                return resolve(data.Items);
            }
        });
    });
}

const getRefs = function(userId) {
    // debug('getRefs', userId);
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: renderTableName('user'),
        IndexName: 'ref-index',
        KeyConditionExpression: `#ref = :user_id`,
        ExpressionAttributeNames: {
            '#ref': 'ref',
        },
        ExpressionAttributeValues: {
            ':user_id': userId
        }
    };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Count));
}

const getNoImgVehicles = require('./operations/get_noimg_vehicles');
const searchVehicles = require('./operations/search_vehicles_query_latlng');
const searchReservations = require('./operations/search_reservations');
const getRentalRequestsByRegNo = require('./operations/get_rental_requests_by_reg_no');
const getRentalRequestsByBookingId = require('./operations/get_rental_requests_by_booking_id');
const getRentalsDue = require('./operations/get_rentals_due');
const getRentalsOlderThan = require('./operations/get_rentals_older_than');
const getUserByStripeAccount = require('./operations/get-user-by-stripe-account');
const getUserByStripeCustomer = require('./operations/get-user-by-stripe-customer');

function searchAvailableVehicles(lat, lng, fromDate, toDate) {
    return Promise.all([searchVehicles(lat, lng), searchReservations(fromDate, toDate)])
        .then(x => {
            // debug(x);
            const reserved = {};
            const vehicles = {};

            for (let i = x[0].length - 1; i >= 0; i--) {
                vehicles[x[0][i].id] = x[0][i];
            }

            // debug('Found vehicles', vehicles);

            for (let i = x[1].length - 1; i >= 0; i--) {
                delete vehicles[x[1][i].vid];
            }

            // debug('Available vehicles', vehicles);

            const available = [];

            for (let v in vehicles) {
                available.push(vehicles[v]);
            }

            return Promise.resolve(available);
        });
}

const removeAttributes = require('./operations/remove_attributes');

function update(table, hash, updates) {
    table = renderTableName(table);

    // debug('update');

    let items = dynasty.table(table);
    updates = cleanProperties(updates);
    if (Object.keys(updates).length === 0) {
        return Promise.resolve();
    }

    return items.update(hash, updates);
}

function deleteReservations(reservations) {
    const table = renderTableName('reservations');

    // debug(`deleteReservations: ${table}`);

    const keys = Object.keys(reservations);

    const promises = [];

    keys.forEach(key => {
        var params = {
            TableName: table,
            Key: {
                id: key,

            }
        };

        var docClient = new AWS.DynamoDB.DocumentClient();
        promises.push(swear(handler => docClient.delete(params, handler)));
    });

    return Promise.all(promises);
}

function expireRental(rental_id) {
    const table = renderTableName('rental_request');

    // debug(`updateRaw: ${table}`);

    var params = {
        TableName: table,
        Key: {
            rental_id: rental_id
        },
        UpdateExpression: 'REMOVE pending, upcoming SET expired = :expired',
        ExpressionAttributeValues: {
            ':expired': new Date().getTime()
        }
    };

    var docClient = new AWS.DynamoDB.DocumentClient();
    return swear(handler => docClient.update(params, handler));
}

function getAll(table, options) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: renderTableName('booking')
    };

    // debug(params);

    return swear(handler => dynamodbDoc.scan(params, handler)).then(data => Promise.resolve(data.Items));
}

const searchByUserId = require('./operations/search_vehicles_by_userid');

function getRentalRequests(keys) {
    let req = {};
    req[renderTableName('rental_request')] = {
        keys: keys
    };

    return new Promise((resolve, reject) => {
        ddb.batchGetItem(req,
            (err, res) => {
                if (err) {
                    return reject(err);
                }

                return resolve(res.items);
            });
    });
}

function getReservations(keys) {
    let req = {};
    req[renderTableName('reservations')] = {
        keys: keys
    };

    return new Promise((resolve, reject) => {
        ddb.batchGetItem(req,
            (err, res) => {
                if (err) {
                    return reject(err);
                }

                return resolve(res.items);
            });
    });
}

function makeReservationsBatch(items) {
    let req = {};
    req[renderTableName('reservations')] = items;

    return new Promise((resolve, reject) => {
        ddb.batchWriteItem(req,
            null,
            (err, res) => {
                if (err) {
                    return reject(err);
                }

                return resolve(res);
            });
    });
}

function makeReservations(items) {
    var _25items = items.splice(0, 25);
    var promises = [];
    while (_25items.length > 0) {
        var promise = makeReservationsBatch(_25items);
        promises.push(promise);
        _25items = items.splice(0, 25);
    }
    return Promise.all(promises);
}

function getItemWithIndex(table, index, hash) {
    table = renderTableName(table);

    return new Promise((resolve, reject) => {
        ddb.query(table, hash, {
            indexName: index,
        }, function(err, item) {
            if (err) {
                return reject(err);
            }

            return resolve(item);
        });
    });
}

function getItemsWithIndex(table, _nooo_, condition) {
    table = renderTableName(table);

    console.log(condition);

    const params = {
        TableName: table,
        ExpressionAttributeValues: {
            ':value': condition.value,
        },
        KeyConditionExpression: `${condition.prop} = :value`,
    };

    var dynamodbDoc = new AWS.DynamoDB.DocumentClient();
    return swear(handler => dynamodbDoc.query(params, handler)).then(result => Promise.resolve(result.Items));
}

function getRentalRequest(rentalId) {
    const id_fragments = rentalId.split('_');

    const rental_id = id_fragments[0];
    const driver_id = id_fragments[1];

    return module.exports.getItem(renderTableName('rental_request'), {
        hash: rental_id,
        range: driver_id
    });
}

function listTables() {
    var db = new AWS.DynamoDB();
    return swear(x => db.listTables(x));
}

function createTables() {
    const tableNames = ['driver_review', 'rental_request', 'reservations', 'user', 'vehicle', 'vehicle_review', 'booking'];
    const tablePrefix = process.env.RBENV ? `${process.env.RBENV}-` : '';

    return listTables().then(tables => {
        const waitList = [];
        tableNames.forEach(tableName => {
            const renderedTableName = renderTableName(tableName);
            if (tables.TableNames.indexOf(renderedTableName) === -1) {
                const wait = require(`./tables/${tableName}`).create(tablePrefix);
                waitList.push(wait);
            } else {
                console.log(`Table '${renderedTableName}' already exists.`);
            }
        });
        return Promise.all(waitList);
    });
}

function updateTables() {
    const tableNames = ['driver_review', 'rental_request', 'reservations', 'user', 'vehicle', 'vehicle_review', 'booking'];
    const tablePrefix = process.env.RBENV ? `${process.env.RBENV}-` : '';

    const waitList = [];

    tableNames.forEach(tableName => {
        const renderedTableName = renderTableName(tableName);
        console.log(`Updating ${tableName}`);
        const wait = () => require(`./tables/${tableName}`).update(tablePrefix);
        waitList.push(wait);
    });

    waitList.forEach(t => t());

    return Promise.all(waitList);
}

function getOwnerRentalRequests(user_id) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: renderTableName('rental_request'),
        IndexName: 'owner-index',
        KeyConditionExpression: `owner_id = :user_id`,
        ExpressionAttributeValues: {
            ':user_id': user_id
        }
    };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items));
}

function getDriverRentalRequests(user_id) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: renderTableName('rental_request'),
        IndexName: 'driver-index',
        KeyConditionExpression: `driver_id = :user_id`,
        ExpressionAttributeValues: {
            ':user_id': user_id
        }
    };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items));
}

function deleteItem(table, key) {
    // debug(`Deleting ${table} ${key}`);

    var params = {
        TableName: renderTableName(table),
        Key: key
    };

    var docClient = new AWS.DynamoDB.DocumentClient();
    return swear(handler => docClient.delete(params, handler));
}

function deleteReservation(reservationId) {
    // debug(`Deleting reservation ${reservationId}`);

    return getItem('reservations', reservationId)
        .then(reservation => {
            if (reservation.rental_id) {
                return Promise.reject(`Reservation ${reservationId} belongs to rental request ${reservation.rental_id} and can not be deleted.`);
            }

            var params = {
                TableName: renderTableName('reservations'),
                Key: {
                    id: reservationId
                }
            };

            var docClient = new AWS.DynamoDB.DocumentClient();
            return swear(handler => docClient.delete(params, handler));
        });
}

module.exports = {
    getNoImgVehicles: getNoImgVehicles,
    getRefs: getRefs,
    deleteReservation: deleteReservation,
    getOwnerRentalRequests: getOwnerRentalRequests,
    getDriverRentalRequests: getDriverRentalRequests,
    getRentalRequest: getRentalRequest,
    getItemsWithIndex: getItemsWithIndex,
    getItemWithIndex: getItemWithIndex,
    makeReservations: makeReservations,
    getReservations: getReservations,
    getRentalRequests: getRentalRequests,
    searchByUserId: searchByUserId,
    getAll: getAll,
    update: update,
    removeAttributes: removeAttributes,
    searchAvailableVehicles,
    searchVehicles: searchVehicles,
    searchReservations: searchReservations,
    getRentalRequestsByRegNo: getRentalRequestsByRegNo,
    getRentalRequestsByBookingId: getRentalRequestsByBookingId,
    getItemWithMap: getItemWithMap,
    getItem: getItem,
    putItem: putItem,
    updateTable: updateTable,
    createTable: createTable,
    listTables: listTables,
    renderTableName: renderTableName,
    createTables: createTables,
    updateTables: updateTables,
    getRentalsDue: getRentalsDue,
    getRentalsOlderThan: getRentalsOlderThan,
    expireRental: expireRental,
    deleteReservations: deleteReservations,
    deleteItem: deleteItem,
    getUserByStripeAccount,
    getUserByStripeCustomer,
};
