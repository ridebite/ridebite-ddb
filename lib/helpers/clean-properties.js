'use strict';

module.exports = function cleanProperties(item) {
    for (let k in item) {
        let current = item[k];

        const isNullOrUndefined = typeof current === 'undefined' || current === null;

        if (isNullOrUndefined || current.length && current.length === 0) {
            delete item[k];
        } else if (typeof current === 'string' || current instanceof Array) {
            if (current.length === 0) {
                delete item[k];
            }
        }
    }
    return item;
}
