const tablePrefix = process.env.RBENV ? `${process.env.RBENV}-` : '';

module.exports = function renderTableName(tableName) {
    return `${tablePrefix}${tableName}`;
};
