'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:remove_attributes');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function removeAttribute(table, key, attributes) {
    table = renderTableName(table);

    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();
    const attributeUpdates = {};

    attributes.forEach(a => {
        attributeUpdates[a] = {
            Action: 'DELETE'
        };
    });

    const params = {
        AttributeUpdates: attributeUpdates,
        TableName: table,
        Key: key
    };

    // debug(params);

    return swear(handler => dynamodbDoc.update(params, handler));
};
