'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:get_rental_requests_by_booking_id');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function searchVehicles(booking_id) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
            TableName: renderTableName('rental_request'),
            IndexName: 'booking-index',
            KeyConditionExpression:`booking_id = :booking_id`,
            ExpressionAttributeValues: {
                ':booking_id': booking_id,
            }
        };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items));
};
