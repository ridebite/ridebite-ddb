'use strict';

const AWS = require('../ddb.js').AWS;
const renderTableName = require('../prefix');

module.exports = function searchVehicles(lat, lng, lastEvaluatedKey) {
    const area = `${lat.toFixed(1)}${lng.toFixed(1)}`;

    console.log(area);

    return new Promise((resolve, reject) => {
        const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

        const params = {
            TableName: renderTableName('vehicle'),
            IndexName: 'area-index',
            ExpressionAttributeValues: {
                ':area': area,
            },
            KeyConditionExpression:`area = :area`,
        };

        if (lastEvaluatedKey) {
            params.ExclusiveStartKey = { id: lastEvaluatedKey };
            console.log(`lastEvaluatedKey: ${params.ExclusiveStartKey}`);
        }

        function onScan(err, data) {
            if (err) {
                console.error('Unable to query. Error:', JSON.stringify(err, null, 2));
                return reject(err);
            } else {
                return resolve(data.Items);
            }
        }

        dynamodbDoc.query(params, onScan);
    });
};
