'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:get-user-by-stripe-account');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function searchVehicles(account) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
            TableName: renderTableName('user'),
            IndexName: 'stripe_account_id-index',
            KeyConditionExpression:`stripe_account_id = :regno`,
            ExpressionAttributeValues: {
                ':regno': account,
            }
        };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items[0]));
};
