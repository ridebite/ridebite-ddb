'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:search_vehicles_query_latlng');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function searchVehicles(lat, lng, lastEvaluatedKey) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const lat_min = lat - 5.0;
    const lat_max = lat + 5.0;
    const lng_min = lng - 5.0;
    const lng_max = lng + 5.0;

    const params = {
        TableName: renderTableName('vehicle'),
        IndexName: 'published-index',
        //ProjectionExpression: 'id, make, model, #yr, price_amount, price_currency, price_unit, avg_rating, lat, lng, fuel, address_label, gearbox, images',
        KeyConditionExpression:`published = :published`,
        FilterExpression: '#lat between :lat_min and :lat_max and #lng between :lng_min and :lng_max',
        ExpressionAttributeNames: {
            '#lat': 'lat',
            '#lng': 'lng'
        },
        ExpressionAttributeValues: {
            ':published': 1,
            ':lat_min': lat_min,
            ':lat_max': lat_max,
            ':lng_min': lng_min,
            ':lng_max': lng_max
        },
    };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items));
};
