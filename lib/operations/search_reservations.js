'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:search_reservations');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function searchVehicles(fromDate, toDate, lastEvaluatedKey) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
            TableName: renderTableName('reservations'),
            IndexName: 'upcoming-index',
            KeyConditionExpression:`upcoming = :upcoming`,
            FilterExpression: '#date between :from_date and :to_date',
            ExpressionAttributeNames: {
                '#date': 'date'
            },
            ExpressionAttributeValues: {
                ':upcoming': 1,
                ':from_date': fromDate,
                ':to_date': toDate
            }
        };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items));
};
