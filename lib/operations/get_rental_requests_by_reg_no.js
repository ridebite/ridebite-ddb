'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:get_rental_requests_by_reg_no');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function searchVehicles(regno) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
            TableName: renderTableName('rental_request'),
            IndexName: 'regno-index',
            KeyConditionExpression:`regno = :regno`,
            ExpressionAttributeValues: {
                ':regno': regno.toUpperCase(),
            }
        };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items));
};
