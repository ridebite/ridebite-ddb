'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:get_noimg_vehicles');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function getNoImagesVehicles(lat, lng) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const lat_min = lat - 5.0;
    const lat_max = lat + 5.0;
    const lng_min = lng - 5.0;
    const lng_max = lng + 5.0;

    const params = {
        TableName: renderTableName('vehicle'),
        IndexName: 'noimage-index',
        //ProjectionExpression: 'id, make, model, #yr, price_amount, price_currency, price_unit, avg_rating, lat, lng, fuel, address_label, gearbox, images, registration_number',
        KeyConditionExpression:`noimage = :noimage`,
        /*ExpressionAttributeNames: {
            '#yr': 'year'
        },*/
        ExpressionAttributeValues: {
            ':noimage': 1
        },
    };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items));
};
