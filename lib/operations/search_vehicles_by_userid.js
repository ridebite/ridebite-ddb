'use strict';

const AWS = require('../ddb.js').AWS;
const renderTableName = require('../prefix');

module.exports = function searchVehicles(userId, lastEvaluatedKey) {
    const limit = 20;
    const fetched = {
        Count: 0,
        LastEvaluatedKey: undefined,
        Items: []
    };

    return new Promise((resolve, reject) => {
        const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

        const params = {
            Limit: limit,
            ProjectionExpression: 'id, make, model, #yr, price_amount, price_currency, price_unit, avg_rating',
            TableName: renderTableName('vehicle'),
            FilterExpression: 'user_id = :uid',
            ExpressionAttributeNames: {
                '#yr': 'year'
            },
            ExpressionAttributeValues: {
                ':uid': userId
            }
        };

        if (lastEvaluatedKey) {
            params.ExclusiveStartKey = { id: lastEvaluatedKey };
            console.log(`lastEvaluatedKey: ${params.ExclusiveStartKey}`);
        }

        function onScan(err, data) {
            if (err) {
                console.error('Unable to query. Error:', JSON.stringify(err, null, 2));
                return reject(err);
            } else {
                fetched.Count += data.Count;
                fetched.Items = fetched.Items.concat(data.Items);
                fetched.LastEvaluatedKey = data.LastEvaluatedKey;

                if (fetched.Count < limit) {
                    //if (typeof data.LastEvaluatedKey != 'undefined') {
                    console.log(`Scanning for more. (${JSON.stringify(data.LastEvaluatedKey)})`);
                    params.ExclusiveStartKey = data.LastEvaluatedKey;
                    return dynamodbDoc.scan(params, onScan);

                    //} else {

                    //}
                }

                return resolve(fetched);
            }
        }

        dynamodbDoc.scan(params, onScan);
    });
};
