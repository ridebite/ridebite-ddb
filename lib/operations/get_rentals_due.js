'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:get_rentals_due');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function getRentalsDue(date) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: renderTableName('rental_request'),
        //ProjectionExpression: 'ShortUrl, RedirectTo, Metadata.CreatedBy',
        FilterExpression: '#datez <= :date AND #upcoming = :upcoming',
        ExpressionAttributeNames: {
            '#datez': 'date',
            '#upcoming': 'upcoming'
        },
        ExpressionAttributeValues: {
            ':upcoming': 1,
            ':date': date
        }
    };

    let returnArray = [];

    return new Promise(resolve => {
        dynamodbDoc.scan(params).eachPage(function(err, data) {
            if (err) {
                throw err;
            }

            if (data === null) {
                return resolve(returnArray);
            }

            returnArray = returnArray.concat(data.Items)
        });
    });
};
