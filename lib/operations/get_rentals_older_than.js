'use strict';

const AWS = require('../ddb.js').AWS;
const debug = require('debug')('ridebite-ddb:search_reservations');
const swear = require('promise-handler').es6;
const renderTableName = require('../prefix');

module.exports = function getRentalsOlderThan(days) {
    const dynamodbDoc = new AWS.DynamoDB.DocumentClient();

    const timestamp = new Date().getTime() - (days * 24 * 60 * 60 * 1000);

    const params = {
            TableName: renderTableName('rental_request'),
            IndexName: 'pending-index',
            KeyConditionExpression:`#pending = :pending`,
            FilterExpression: '#requested <= :requested',
            ExpressionAttributeNames: {
                '#pending': 'pending',
                '#requested': 'requested',
            },
            ExpressionAttributeValues: {
                ':pending': 1,
                ':requested': timestamp
            }
        };

    // debug(params);

    return swear(handler => dynamodbDoc.query(params, handler)).then(data => Promise.resolve(data.Items));
};
