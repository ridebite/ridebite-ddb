const ddb = require('../ddb');

const provisionedThroughput = {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
};

const globalSecondaryIndexes = [{
    IndexName: 'ref-index',
    KeySchema: [{
        AttributeName: 'ref',
        KeyType: 'HASH'
    }],
    Projection: {
        ProjectionType: 'KEYS_ONLY'
    },
    ProvisionedThroughput: provisionedThroughput
}, {
    IndexName: 'stripe_account_id-index',
    KeySchema: [{
        AttributeName: 'stripe_account_id',
        KeyType: 'HASH'
    }],
    Projection: {
        ProjectionType: 'ALL'
    },
    ProvisionedThroughput: provisionedThroughput
}, {
    IndexName: 'stripe_cus-index',
    KeySchema: [{
        AttributeName: 'stripe_cus',
        KeyType: 'HASH'
    }],
    Projection: {
        ProjectionType: 'ALL'
    },
    ProvisionedThroughput: provisionedThroughput
}];

const attributeDefinitions = [{
    AttributeName: 'user_id',
    AttributeType: 'S'
}, {
    AttributeName: 'ref',
    AttributeType: 'S'
}];

module.exports.create = function(tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}user`,
        KeySchema: [{
            AttributeName: 'user_id',
            KeyType: 'HASH'
        }],
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput,
        GlobalSecondaryIndexes: globalSecondaryIndexes
    };

    return ddb.createTable(schema);
};

module.exports.update = function(tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}user`,
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput
    };

    return ddb.updateTable(schema);
};
