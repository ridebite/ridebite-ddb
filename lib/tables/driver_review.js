const ddb = require('../ddb');

const provisionedThroughput = {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
};

const attributeDefinitions = [
            { AttributeName: 'driver_id', AttributeType: 'S' },
            { AttributeName: 'rental_id', AttributeType: 'S' }
        ];

module.exports.create = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}driver_review`,
        KeySchema: [
            { AttributeName: 'driver_id', KeyType: 'HASH' },
            { AttributeName: 'rental_id', KeyType: 'RANGE' }
        ],
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput
    };

    return ddb.createTable(schema);
};

module.exports.update = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}driver_review`,
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput
    };

    return ddb.updateTable(schema);
};
