const ddb = require('../ddb');

const provisionedThroughput = {
    ReadCapacityUnits: 2,
    WriteCapacityUnits: 1
};

const globalSecondaryIndexes = [/*{
            IndexName: 'date-index',
            KeySchema: [{
                AttributeName: 'date',
                KeyType: 'HASH'
            }],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: provisionedThroughput
        }, */{
            IndexName: 'upcoming-index',
            KeySchema: [{
                AttributeName: 'upcoming',
                KeyType: 'HASH'
            }],
            Projection: {
                ProjectionType: 'KEYS_ONLY'
            },
            ProvisionedThroughput: provisionedThroughput
        }, {
            IndexName: 'pending-index',
            KeySchema: [{
                AttributeName: 'pending',
                KeyType: 'HASH'
            }],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: provisionedThroughput
        }, /* {
            IndexName: 'regno-index',
            KeySchema: [{
                AttributeName: 'regno',
                KeyType: 'HASH'
            }],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: provisionedThroughput
        },*/ {
            IndexName: 'driver-index',
            KeySchema: [{
                AttributeName: 'driver_id',
                KeyType: 'HASH'
            }],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: provisionedThroughput
        },{
            IndexName: 'booking-index',
            KeySchema: [{
                AttributeName: 'booking_id',
                KeyType: 'HASH'
            }],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: provisionedThroughput
        }, {
            IndexName: 'owner-index',
            KeySchema: [{
                AttributeName: 'owner_id',
                KeyType: 'HASH'
            }],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: provisionedThroughput
        }];

const attributeDefinitions = [
{
    AttributeName: 'rental_id',
    AttributeType: 'S'
},{
    AttributeName: 'driver_id',
    AttributeType: 'S'
},{
    AttributeName: 'booking_id',
    AttributeType: 'S'
},{
    AttributeName: 'owner_id',
    AttributeType: 'S'
}, /*{
    AttributeName: 'date',
    AttributeType: 'N'
},*/ {
    AttributeName: 'upcoming',
    AttributeType: 'N'
}, {
    AttributeName: 'pending',
    AttributeType: 'N'
}/*, {
    AttributeName: 'regno',
    AttributeType: 'S'
}*/];

module.exports.create = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}rental_request`,
        KeySchema: [{
            AttributeName: 'rental_id',
            KeyType: 'HASH'
        }],
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput,
        GlobalSecondaryIndexes: globalSecondaryIndexes
    };

    return ddb.createTable(schema);
};

module.exports.update = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}rental_request`,
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput,
        GlobalSecondaryIndexUpdates: []
    };

    globalSecondaryIndexes.forEach(gi => {
        delete gi.KeySchema;
        delete gi.Projection;
        schema.GlobalSecondaryIndexUpdates.push({
            Update: gi
        });
    });

    return ddb.updateTable(schema);
};
