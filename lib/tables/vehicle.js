const ddb = require('../ddb');

const provisionedThroughput = {
    ReadCapacityUnits: 2,
    WriteCapacityUnits: 1
};

const globalSecondaryIndexes = [
            {
                IndexName: 'published-index',
                KeySchema: [{ AttributeName: 'published', KeyType: 'HASH' }],
                Projection: {
                    ProjectionType: 'ALL'
                },
                ProvisionedThroughput: {
                    ReadCapacityUnits: 1,
                    WriteCapacityUnits: 1
                }
            },
            {
                IndexName: 'noimage-index',
                KeySchema: [{ AttributeName: 'noimage', KeyType: 'HASH' }],
                Projection: {
                    ProjectionType: 'ALL'
                },
                ProvisionedThroughput: {
                    ReadCapacityUnits: 1,
                    WriteCapacityUnits: 1
                }
            }
        ];

const attributeDefinitions = [
            { AttributeName: 'id', AttributeType: 'S' },
            { AttributeName: 'published', AttributeType: 'N' },
            { AttributeName: 'noimage', AttributeType: 'N' },
        ];

module.exports.create = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}vehicle`,
        KeySchema: [
            { AttributeName: 'id', KeyType: 'HASH' }
        ],
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput,
        GlobalSecondaryIndexes: globalSecondaryIndexes
    };

    return ddb.createTable(schema);
};

module.exports.update = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}vehicle`,
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput,
        GlobalSecondaryIndexUpdates: []
    };

    globalSecondaryIndexes.forEach(gi => {
        delete gi.KeySchema;
        delete gi.Projection;
        schema.GlobalSecondaryIndexUpdates.push({
            Update: gi
        });
    });

    return ddb.updateTable(schema);
};
