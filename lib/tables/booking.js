const ddb = require('../ddb');

const provisionedThroughput = {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
};

//const globalSecondaryIndexes = [{
//            IndexName: 'ref-index',
//            KeySchema: [{
//                AttributeName: 'ref',
//                KeyType: 'HASH'
//            }],
//            Projection: {
//                ProjectionType: 'KEYS_ONLY'
//            },
//            ProvisionedThroughput: provisionedThroughput
//        }];
//
const attributeDefinitions = [
            { AttributeName: 'booking_id', AttributeType: 'S' }
        ];

module.exports.create = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}booking`,
        KeySchema: [
            { AttributeName: 'booking_id', KeyType: 'HASH' }
        ],
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput
    };

    return ddb.createTable(schema);
};

module.exports.update = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}booking`,
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput
    };

    return ddb.updateTable(schema);
};
