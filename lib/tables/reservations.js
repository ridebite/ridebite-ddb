const ddb = require('../ddb');

const provisionedThroughput = {
    ReadCapacityUnits: 2,
    WriteCapacityUnits: 1
};

const globalSecondaryIndexes = [{
            IndexName: 'upcoming-index',
            KeySchema: [{
                AttributeName: 'upcoming',
                KeyType: 'HASH'
            }],
            Projection: {
                ProjectionType: 'ALL'
            },
            ProvisionedThroughput: provisionedThroughput
        }];

const attributeDefinitions = [{
    AttributeName: 'id',
    AttributeType: 'S'
}, {
    AttributeName: 'upcoming',
    AttributeType: 'N'
}];

module.exports.create = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}reservations`,
        KeySchema: [{
            AttributeName: 'id',
            KeyType: 'HASH'
        }],
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: {
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1
        },
        GlobalSecondaryIndexes: globalSecondaryIndexes
    };

    return ddb.createTable(schema);
};

module.exports.update = function (tablePrefix) {
    const schema = {
        TableName: `${tablePrefix}reservations`,
        AttributeDefinitions: attributeDefinitions,
        ProvisionedThroughput: provisionedThroughput,
        GlobalSecondaryIndexUpdates: []
    };

    globalSecondaryIndexes.forEach(gi => {
        delete gi.KeySchema;
        delete gi.Projection;
        schema.GlobalSecondaryIndexUpdates.push({
            Update: gi
        });
    });

    return ddb.updateTable(schema);
};
